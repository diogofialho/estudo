public class Fracoes implements Comparable<Fracoes>{

    int numerador,denominador;
    int numIrr,denIrr;
    Fatorial fatorial = new Fatorial();

    public Fracoes(){}

    public Fracoes(int numerador, int denominador){
        this.numerador = numerador;
        this.denominador = denominador;
        reduce();
    }

    public Fracoes(int numerador){
        this.numerador = numerador;
        this.denominador = 1;
        reduce();
    }


    private void reduce(){
        int mdc = fatorial.mdcRec(numerador,denominador);
        if(numerador==denominador){
            this.numIrr = 1;
            this.denIrr = 1;
        }else {
            this.numIrr = numerador / mdc;
            this.denIrr = denominador / mdc;
        }
    }

    public String toString(){
        if(numIrr == 0)
            return "0";
        if(numIrr == denIrr)
            return "1";
        if(denIrr == 1)
            return String.valueOf(numIrr);
        if(denIrr != 0){
            return String.valueOf(numIrr) +"/" +String.valueOf(denIrr);
        }else
            return "UNDEFINED";
    }

    public Fracoes sum(Fracoes fraction){
        int newNum = this.numIrr * fraction.getDenominador() + this.denIrr * fraction.getNumerador();
        int newDen = this.denIrr * fraction.getDenominador();
        return new Fracoes(newNum,newDen);
    }

    public Fracoes product(Fracoes fraction){
        int newNum = this.numIrr * fraction.getNumerador();
        int newDen = this.denIrr * fraction.getDenominador();
        return new Fracoes(newNum,newDen);
    }

    public Fracoes div(Fracoes fraction){
        return this.product(new Fracoes(fraction.getDenominador(),fraction.getNumerador()));
    }

    public int getNumerador(){
        return numIrr;
    }

    public float getValue(){
        return (float)(numerador / denominador);
    }

    public int getDenominador(){
        return denIrr;
    }

    @Override
    public int compareTo(Fracoes fraction) {
        if(fraction == null)
            throw new NullPointerException();

    }
}
